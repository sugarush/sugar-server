import asyncio

from quart import Quart

from sugar_api import CORS
from sugar_concache.mongo import MongoDB
from sugar_concache.redis import RedisDB


server = Quart('application-name')
'''
The Quart server object.
'''

CORS.set_origins('*')

RedisDB.defaults = {
    'host': 'redis://localhost',
    'minsize': 5,
    'maxsize': 10
}

@server.before_serving
async def setup_database_connections():
    '''
    Set `MongoDB` and `RedisDB` event loops to the `Quart` event loop.

    .. attention::

        You should not call this function, it is called automatically.
    '''
    loop = asyncio.get_event_loop()
    MongoDB.set_event_loop(loop)
    await RedisDB.set_event_loop(loop)

@server.after_serving
async def close_database_connections():
    '''
    Close all `MongoDB` and `RedisDB` connections before the server stops.

    .. attention::

        You should not call this function, it is called automatically.
    '''
    MongoDB.close()
    await RedisDB.close()
