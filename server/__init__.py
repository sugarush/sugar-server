from os import listdir
from os.path import dirname, join
from importlib import import_module

files = filter(lambda file: file.endswith('.py'), \
    listdir(join(dirname(__file__), 'routes')))

for file in files:
    module = file.split('.')[0]
    import_module(f'.{module}', package=f'server.routes')

from . import resource
from . import seed
from . import log

from . server import server
