import argparse

from server import server

parser = argparse.ArgumentParser(description='Sugar Server')

parser.add_argument('--host', default='0.0.0.0')
parser.add_argument('--port', type=int, default=8000)
parser.add_argument('--debug', action='store_const', const=True, default=False)

args = parser.parse_args()

server.run(host=args.host, port=args.port, debug=args.debug)
